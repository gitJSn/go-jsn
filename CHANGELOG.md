# [1.5.0](https://gitlab.com/gitJSn/go-jsn/compare/v1.4.0...v1.5.0) (2024-06-05)


### Features

* update deps ([b25f994](https://gitlab.com/gitJSn/go-jsn/commit/b25f994527184cf4c58c5db0df8830b3c76409b8))

# [1.4.0](https://gitlab.com/gitJSn/go-jsn/compare/v1.3.1...v1.4.0) (2024-06-05)


### Features

* clients should create publishers themselves and add them using AddPublisher ([f8e525b](https://gitlab.com/gitJSn/go-jsn/commit/f8e525b7b208585586005550f89bdec420cc2bcd))

## [1.3.1](https://gitlab.com/gitJSn/go-jsn/compare/v1.3.0...v1.3.1) (2024-05-29)


### Bug Fixes

* nil pointer ([d78ba41](https://gitlab.com/gitJSn/go-jsn/commit/d78ba414830c9488b69f28a163497fcb1c80cd03))

# [1.3.0](https://gitlab.com/gitJSn/go-jsn/compare/v1.2.1...v1.3.0) (2024-05-28)


### Features

* add CloseConsumer Method, use logrus debug even for rabbitmq Info logs ([999a1cb](https://gitlab.com/gitJSn/go-jsn/commit/999a1cb67b59fb8573aeb47a0082facfc84d5335))

## [1.2.1](https://gitlab.com/gitJSn/go-jsn/compare/v1.2.0...v1.2.1) (2024-05-25)


### Bug Fixes

* producer -> publisher ([8ad66d4](https://gitlab.com/gitJSn/go-jsn/commit/8ad66d4df1267304bf906222cc2b26f286b4327a))

# [1.2.0](https://gitlab.com/gitJSn/go-jsn/compare/v1.1.0...v1.2.0) (2024-05-24)


### Features

* switch to logrus ([e2400af](https://gitlab.com/gitJSn/go-jsn/commit/e2400af5aa12dc0fdc0e8847d73984309f668860))

# [1.1.0](https://gitlab.com/gitJSn/go-jsn/compare/v1.0.0...v1.1.0) (2024-05-24)


### Features

* add health package, add logrus go-rabbitmq logger ([d76be0f](https://gitlab.com/gitJSn/go-jsn/commit/d76be0f2ace6e5183314d155d193e58f22093925))

# 1.0.0 (2024-05-22)


### Features

* base functionality for rabbitext ([d65b5a3](https://gitlab.com/gitJSn/go-jsn/commit/d65b5a34e814d4dd888b6d8cc0a8cb6483cfb728))

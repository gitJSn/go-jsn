package rabbitext

import (
	"fmt"

	log "github.com/sirupsen/logrus"
)

const loggingPrefix = "[rabbitext]"

type RabbitLogrus struct{}

// Fatalf -
func (l RabbitLogrus) Fatalf(format string, v ...interface{}) {
	log.Fatalf(fmt.Sprintf("%s - %s", loggingPrefix, format), v...)
}

// Errorf -
func (l RabbitLogrus) Errorf(format string, v ...interface{}) {
	log.Errorf(fmt.Sprintf("%s - %s", loggingPrefix, format), v...)
}

// Warnf -
func (l RabbitLogrus) Warnf(format string, v ...interface{}) {
	log.Warnf(fmt.Sprintf("%s - %s", loggingPrefix, format), v...)
}

// Infof - (we always use debug for rabbitmq logs on purpose)
func (l RabbitLogrus) Infof(format string, v ...interface{}) {
	log.Debugf(fmt.Sprintf("%s - %s", loggingPrefix, format), v...)
}

// Debugf -
func (l RabbitLogrus) Debugf(format string, v ...interface{}) {
	log.Debugf(fmt.Sprintf("%s - %s", loggingPrefix, format), v...)
}

package rabbitext

import (
	log "github.com/sirupsen/logrus"
	"github.com/wagslane/go-rabbitmq"
)

type MQ struct {
	Conn       *rabbitmq.Conn
	consumers  []*rabbitmq.Consumer
	publishers []*rabbitmq.Publisher
	Logger     rabbitmq.Logger
}

func NewMQ(url string) (*MQ, error) {
	logger := RabbitLogrus{}

	conn, err := rabbitmq.NewConn(
		url,
		rabbitmq.WithConnectionOptionsLogging,
		rabbitmq.WithConnectionOptionsLogger(logger),
	)
	if err != nil {
		return nil, err
	}

	return &MQ{
		Conn:   conn,
		Logger: logger,
	}, nil
}

func (mq *MQ) IsReady() bool {
	return mq.Conn != nil
}

func (mq *MQ) addConsumer(consumer *rabbitmq.Consumer) {
	mq.consumers = append(mq.consumers, consumer)
}

func (mq *MQ) removeConsumer(consumer *rabbitmq.Consumer) {
	for i, c := range mq.consumers {
		if c == consumer {
			mq.consumers = append(mq.consumers[:i], mq.consumers[i+1:]...)
			break
		}
	}
}

func (mq *MQ) RunConsumer(consumer *rabbitmq.Consumer, handler rabbitmq.Handler) {
	go consumer.Run(handler)
	mq.addConsumer(consumer)
}

func (mq *MQ) AddPublisher(publisher *rabbitmq.Publisher) {
	mq.publishers = append(mq.publishers, publisher)
}

func (mq *MQ) Close() {
	log.Info("Closing message queue consumers...")
	for _, consumer := range mq.consumers {
		consumer.Close()
	}

	log.Info("Closing message queue publishers...")
	for _, publisher := range mq.publishers {
		publisher.Close()
	}

	log.Info("Closing message queue connection...")
	mq.Conn.Close()
}

func (mq *MQ) CloseConsumer(consumer *rabbitmq.Consumer) {
	consumer.Close()
	mq.removeConsumer(consumer)
}

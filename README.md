# go-jsn

## Install

```bash
go get gitlab.com/gitJSn/go-jsn
```

## rabbitext

### Setup

- Create a mq instance in your main function
- Create a graceful shutdown and include mq.Close() in the cleanup code
- Example:

```go
package main

func main() {
    // Initialize RabbitMQ
    mq, err := rabbitext.NewMQ(config.GetRabbitMQChannel())
    if err != nil {
        log.Fatal(fmt.Errorf("error initializing RabbitMQ: %v", err))
    }

    // ... your setup code

    // Cleanup
    ctx, cancel := context.WithCancel(context.Background())
    defer cancel()

    // Channel to handle signals
    sigs := make(chan os.Signal, 1)
    signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

    go func() {
        sig := <-sigs
        // ... other cleanup code

        // Close the RabbitMQ connection
        mq.Close()
        cancel()
    }()

    // Bottom of main function
    <-ctx.Done()
}
```

### Define Consumers

- Create a messaging package in your project
- Define consumers to use in your application
- Example:

```go
package messaging

import (
	"gitlab.com/gitJSn/go-jsn/rabbitext"
	"github.com/wagslane/go-rabbitmq"
)

func TargetOnlineConsumer(c *rabbitext.MQ) (*rabbitmq.Consumer, error) {
	return rabbitmq.NewConsumer(
		c.Conn,
		"analyzer.contact.online.processing", // queue name
		rabbitmq.WithConsumerOptionsRoutingKey("contact.online"), // routing key
		rabbitmq.WithConsumerOptionsExchangeName("mdh.customer-exchange"), // exchange name
		rabbitmq.WithConsumerOptionsExchangeDeclare,
		rabbitmq.WithConsumerOptionsExchangeDurable,
	)
}

// ... other consumers
```

### Start Consuming

- In you service create a consumer from your messaging package
- Start consuming messages using mq.RunConsumer
- Example:

```go
package main

func SomeService(
    // ... other parameters
    mq *rabbitext.MQ,
) {
    // ... other setup code

    // Create consumer from messaging package
    consumer, err := messaging.TargetOnlineConsumer(mq)
    if err != nil {
        log.Fatal(fmt.Errorf("error initializing TargetOnlineConsumer: %v", err))
    }

    // Start consuming
	mq.RunConsumer(consumer, func(d rabbitmq.Delivery) (action rabbitmq.Action) {
        // ... your consumer code
        // return rabbitmq.Ack to acknowledge the message
        // return rabbitmq.NackDiscard to discard the message
        // return rabbitmq.NackRequeue to requeue the message
        return rabbitmq.Ack
    })

    // ... other setup code
}
```

### Publish Messages

- Define publishers like the consumers or use the default mq.NewPublisher (less flexible)
- Create the publisher in your service
- Publish messages using mq.Publish
- Make sure to reuse publishers (i usually create one per service and hold it in the service struct)
- Example:

```go
package main

func SomeService(
    // ... other parameters
    mq *rabbitext.MQ,
) {
    // ... other setup code

    // Create publisher
    publisher, err := mq.NewPublisher("mdh.customer-exchange")
    if err != nil {
        log.Fatal(fmt.Errorf("error initializing Publisher: %v", err))
    }

    // Publish message
    err = publisher.Publish(
        []byte("Hello, World!"), // the data
        []string{"routing-key"}, // makes sure the message is routed to the correct queue
        rabbitmq.WithPublishOptionsPersistentDelivery, // optional options
		rabbitmq.WithPublishOptionsContentType("application/json"),
		rabbitmq.WithPublishOptionsExchange("some-exchange"), // exchange for clarity
    )
    if err != nil {
        log.Fatal(fmt.Errorf("error publishing message: %v", err))
    }

    // ... other setup code
}
```

package health

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/gitJSn/go-jsn/rabbitext"

	"github.com/gin-gonic/gin"
	"github.com/redis/go-redis/v9"
	"gorm.io/gorm"
)

type HealthHandler struct {
	db           *gorm.DB
	mq           *rabbitext.MQ
	redis        *redis.Client
	customChecks map[string]func() (bool, error)
}

func NewHealthHandler(db *gorm.DB, mq *rabbitext.MQ, redis *redis.Client) *HealthHandler {
	return &HealthHandler{
		db:           db,
		mq:           mq,
		redis:        redis,
		customChecks: make(map[string]func() (bool, error)),
	}
}

func (h *HealthHandler) AddCustomCheck(name string, check func() (bool, error)) {
	h.customChecks[name] = check
}

func (h *HealthHandler) RegisterRoutes(r *gin.Engine) {
	r.GET("/health/ready", h.ReadinessCheck)
	r.GET("/health/live", h.LivenessCheck)
}

func (h *HealthHandler) LivenessCheck(c *gin.Context) {
	c.JSON(200, gin.H{"status": "alive"})
}

func (h *HealthHandler) ReadinessCheck(c *gin.Context) {
	ctx := context.Background()

	if ok, err := h.checkGormReadiness(ctx); !ok {
		c.JSON(500, gin.H{"status": "error", "message": "gorm is not ready", "error": err.Error()})
		return
	}

	if ok, err := h.checkRedisReadiness(ctx); !ok {
		c.JSON(500, gin.H{"status": "error", "message": "redis is not ready", "error": err.Error()})
		return
	}

	if ok, err := h.checkMqReadiness(); !ok {
		c.JSON(500, gin.H{"status": "error", "message": "message queue is not ready", "error": err.Error()})
		return
	}

	for name, check := range h.customChecks {
		if ok, _ := check(); !ok {
			message := fmt.Sprintf("custom resource %s is not ready", name)
			c.JSON(500, gin.H{"status": "error", "message": message})
			return
		}
	}

	c.JSON(200, gin.H{"status": "ready"})
}

func (h *HealthHandler) checkMqReadiness() (bool, error) {
	if h.mq == nil {
		return true, nil
	}

	if !h.mq.IsReady() {
		return false, nil
	}

	return true, nil
}

func (h *HealthHandler) checkGormReadiness(ctx context.Context) (bool, error) {
	if h.db == nil {
		return true, nil
	}

	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	if err := h.db.WithContext(ctx).Exec("SELECT 1").Error; err != nil {
		return false, err
	}

	return true, nil
}

func (h *HealthHandler) checkRedisReadiness(ctx context.Context) (bool, error) {
	if h.redis == nil {
		return true, nil
	}

	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	if err := h.redis.Ping(ctx).Err(); err != nil {
		return false, err
	}

	return true, nil
}
